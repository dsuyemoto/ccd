﻿# Created 10/30/15 Douglas Suyemoto

Add-Type @"
using System;
using System.Runtime.InteropServices;

namespace CCD
{
    public class ChangeDisplay
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct LUID
        {
            public uint LowPart;
            public uint HighPart;
        }

        [Flags]
        public enum DisplayConfigVideoOutputTechnology : uint
        {
            Other = 4294967295, // -1
            Hd15 = 0,
            Svideo = 1,
            CompositeVideo = 2,
            ComponentVideo = 3,
            Dvi = 4,
            Hdmi = 5,
            Lvds = 6,
            DJpn = 8,
            Sdi = 9,
            DisplayportExternal = 10,
            DisplayportEmbedded = 11,
            UdiExternal = 12,
            UdiEmbedded = 13,
            Sdtvdongle = 14,
            Internal = 0x80000000,
            ForceUint32 = 0xFFFFFFFF
        }

        [Flags]
        public enum SdcFlags : uint
        {
            Zero = 0,

            TopologyInternal = 0x00000001,
            TopologyClone = 0x00000002,
            TopologyExtend = 0x00000004,
            TopologyExternal = 0x00000008,
            TopologySupplied = 0x00000010,

            UseSuppliedDisplayConfig = 0x00000020,
            Validate = 0x00000040,
            Apply = 0x00000080,
            NoOptimization = 0x00000100,
            SaveToDatabase = 0x00000200,
            AllowChanges = 0x00000400,
            PathPersistIfRequired = 0x00000800,
            ForceModeEnumeration = 0x00001000,
            AllowPathOrderChanges = 0x00002000,

            UseDatabaseCurrent = TopologyInternal | TopologyClone | TopologyExtend | TopologyExternal
        }

        [Flags]
        public enum DisplayConfigSourceStatus
        {
            Zero = 0x0,
            InUse = 0x00000001
        }

        [Flags]
        public enum DisplayConfigTargetStatus : uint
        {
            Zero = 0x0,

            InUse = 0x00000001,
            FORCIBLE = 0x00000002,
            ForcedAvailabilityBoot = 0x00000004,
            ForcedAvailabilityPath = 0x00000008,
            ForcedAvailabilitySystem = 0x00000010,
        }

        [Flags]
        public enum DisplayConfigRotation : uint
        {
            Zero = 0x0,

            Identity = 1,
            Rotate90 = 2,
            Rotate180 = 3,
            Rotate270 = 4,
            ForceUint32 = 0xFFFFFFFF
        }

        [Flags]
        public enum DisplayConfigScaling : uint
        {
            Zero = 0x0,

            Identity = 1,
            Centered = 2,
            Stretched = 3,
            Aspectratiocenteredmax = 4,
            Custom = 5,
            Preferred = 128,
            ForceUint32 = 0xFFFFFFFF
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigRational
        {
            public uint numerator;
            public uint denominator;
        }

        [Flags]
        public enum DisplayConfigScanLineOrdering : uint
        {
            Unspecified = 0,
            Progressive = 1,
            Interlaced = 2,
            InterlacedUpperfieldfirst = Interlaced,
            InterlacedLowerfieldfirst = 3,
            ForceUint32 = 0xFFFFFFFF
        }

        [Flags]
        public enum DisplayConfigModeInfoType : uint
        {
            Zero = 0,

            Source = 1,
            Target = 2,
            ForceUint32 = 0xFFFFFFFF
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfig2DRegion
        {
            public uint cx;
            public uint cy;
        }

        [Flags]
        public enum D3DmdtVideoSignalStandard : uint
        {
            Uninitialized = 0,
            VesaDmt = 1,
            VesaGtf = 2,
            VesaCvt = 3,
            Ibm = 4,
            Apple = 5,
            NtscM = 6,
            NtscJ = 7,
            Ntsc443 = 8,
            PalB = 9,
            PalB1 = 10,
            PalG = 11,
            PalH = 12,
            PalI = 13,
            PalD = 14,
            PalN = 15,
            PalNc = 16,
            SecamB = 17,
            SecamD = 18,
            SecamG = 19,
            SecamH = 20,
            SecamK = 21,
            SecamK1 = 22,
            SecamL = 23,
            SecamL1 = 24,
            Eia861 = 25,
            Eia861A = 26,
            Eia861B = 27,
            PalK = 28,
            PalK1 = 29,
            PalL = 30,
            PalM = 31,
            Other = 255
        }

        [Flags]
        public enum QueryDisplayFlags : uint
        {
            Zero = 0x0,

            AllPaths = 0x00000001,
            OnlyActivePaths = 0x00000002,
            DatabaseCurrent = 0x00000004
        }

        [Flags]
        public enum DisplayConfigPixelFormat : uint
        {
            Zero = 0x0,

            Pixelformat8Bpp = 1,
            Pixelformat16Bpp = 2,
            Pixelformat24Bpp = 3,
            Pixelformat32Bpp = 4,
            PixelformatNongdi = 5,
            PixelformatForceUint32 = 0xffffffff
        }

        [Flags]
        public enum DisplayConfigTopologyId : uint
        {
            Zero = 0x0,

            Internal = 0x00000001,
            Clone = 0x00000002,
            Extend = 0x00000004,
            External = 0x00000008,
            ForceUint32 = 0xFFFFFFFF
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct PointL
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigVideoSignalInfo
        {
            public long pixelRate;
            public DisplayConfigRational hSyncFreq;
            public DisplayConfigRational vSyncFreq;
            public DisplayConfig2DRegion activeSize;
            public DisplayConfig2DRegion totalSize;

            public D3DmdtVideoSignalStandard videoStandard;
            public DisplayConfigScanLineOrdering ScanLineOrdering;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigTargetMode
        {
            public DisplayConfigVideoSignalInfo targetVideoSignalInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigPathSourceInfo
        {
            public LUID adapterId;
            public uint id;
            public uint modeInfoIdx;

            public DisplayConfigSourceStatus statusFlags;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigPathTargetInfo
        {
            public LUID adapterId;
            public uint id;
            public uint modeInfoIdx;
            public DisplayConfigVideoOutputTechnology outputTechnology;
            public DisplayConfigRotation rotation;
            public DisplayConfigScaling scaling;
            public DisplayConfigRational refreshRate;
            public DisplayConfigScanLineOrdering scanLineOrdering;

            public bool targetAvailable;
            public DisplayConfigTargetStatus statusFlags;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigPathInfo
        {
            public DisplayConfigPathSourceInfo sourceInfo;
            public DisplayConfigPathTargetInfo targetInfo;
            public uint flags;
        }


        [StructLayout(LayoutKind.Sequential)]
        public struct DisplayConfigSourceMode
        {
            public uint width;
            public uint height;
            public DisplayConfigPixelFormat pixelFormat;
            public PointL position;
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct DisplayConfigModeInfo
        {
            [FieldOffset((0))]
            public DisplayConfigModeInfoType infoType;

            [FieldOffset(4)]
            public uint id;

            [FieldOffset(8)]
            public LUID adapterId;

            [FieldOffset(16)]
            public DisplayConfigTargetMode targetMode;

            [FieldOffset(16)]
            public DisplayConfigSourceMode sourceMode;
        }

        [DllImport("User32.dll")]
        private static extern int SetDisplayConfig(
                    int numPathArrayElements,
                    [In] DisplayConfigPathInfo[] pathArray,
                    int numModeInfoArrayElements,
                    [In] DisplayConfigModeInfo[] modeInfoArray,
                    SdcFlags flags);

        [DllImport("User32.dll")]
        private static extern int QueryDisplayConfig(
                    QueryDisplayFlags flags,
                    ref int numPathArrayElements,
                    [Out] DisplayConfigPathInfo[] pathInfoArray,
                    ref int modeInfoArrayElements,
                    [Out] DisplayConfigModeInfo[] modeInfoArray,
                    out DisplayConfigTopologyId currentTopologyId);

        [DllImport("User32.dll")]
        private static extern int GetDisplayConfigBufferSizes(
                    QueryDisplayFlags flags,
                    out int numPathArrayElements,
                    out int numModeInfoArrayElements);

        static int numPathArrayElements;
        static int numModeInfoArrayElements;

        public static void SetPrimaryDisplay(string display)
        {
            if (String.IsNullOrEmpty(display))
                return;

            // query active paths from the current computer.
            if (GetDisplayConfigBufferSizes(QueryDisplayFlags.OnlyActivePaths, out numPathArrayElements,
                                                           out numModeInfoArrayElements) == 0)
            {
                // 0 is success.
                var pathInfoArray = new DisplayConfigPathInfo[numPathArrayElements];
                var modeInfoArray = new DisplayConfigModeInfo[numModeInfoArrayElements];
                var currentTopologyId = DisplayConfigTopologyId.Internal;

                var first = Marshal.SizeOf(new DisplayConfigPathInfo());
                var second = Marshal.SizeOf(new DisplayConfigModeInfo());

                QueryDisplayConfig(QueryDisplayFlags.DatabaseCurrent,
                                        ref numPathArrayElements, pathInfoArray, ref numModeInfoArrayElements,
                                        modeInfoArray, out currentTopologyId);

                DisplayConfigVideoOutputTechnology displayMap;
                
                Enum.TryParse(display, out displayMap);

                uint setToPrimaryIndex = 0;
                uint setToSecondaryIndex = 0;

                foreach (var output in pathInfoArray)
                {
                    if (output.sourceInfo.statusFlags == DisplayConfigSourceStatus.InUse) 
                    {
                        if (output.targetInfo.outputTechnology == displayMap)
                        {
                            setToPrimaryIndex = output.sourceInfo.modeInfoIdx;
                        } else {
                            setToSecondaryIndex = output.sourceInfo.modeInfoIdx; 
                        }
                    }
                }

                string primaryMonitor = string.Empty;

                if (modeInfoArray[setToPrimaryIndex].sourceMode.position.x == 0 && modeInfoArray[setToPrimaryIndex].sourceMode.position.y == 0)
                    return;

                modeInfoArray[setToPrimaryIndex].sourceMode.position.x = 0;
                modeInfoArray[setToPrimaryIndex].sourceMode.position.y = 0;
                modeInfoArray[setToSecondaryIndex].sourceMode.position.x = (int)modeInfoArray[setToPrimaryIndex].sourceMode.width;
                modeInfoArray[setToSecondaryIndex].sourceMode.position.y = 0;

                var status = SetDisplayConfig(numPathArrayElements, pathInfoArray, numModeInfoArrayElements, modeInfoArray,
                    SdcFlags.UseSuppliedDisplayConfig | SdcFlags.Apply | SdcFlags.SaveToDatabase);
             
            }
        }
    }  
}
"@

# Choose the output that should be primary

$Other = "Other"
$Hd15 = "Hd15"
$Svideo = "Svideo"
$CompositeVideo = "CompositeVideo"
$ComponentVideo = "ComponentVideo"
$Dvi = "Dvi"
$Hdmi = "Hdmi"
$Lvds = "Lvds"
$DJpn = "DJpn"
$Sdi = "Sdi"
$DisplayportExternal = "DisplayportExternal"
$DisplayportEmbedded = "DisplayportEmbedded"
$UdiExternal = "UdiExternal"
$UdiEmbedded = "UdiEmbedded"
$Sdtvdongle = "Sdtvdongle"
$Internal = "Internal"

[CCD.ChangeDisplay]::SetPrimaryDisplay( $Hd15 )
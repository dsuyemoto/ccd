﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ChangeWindowsDisplay
{
    class Program
    {
        static void Main(string[] args)
        {

            CCD.ChangeDisplay.SetPrimaryDisplay(CCD.ChangeDisplay.DisplayConfigVideoOutputTechnology.Hd15.ToString());

            int numPathArrayElements;
            int numModeInfoArrayElements;

            // query active paths from the current computer.
            if (CCDWrapper.GetDisplayConfigBufferSizes(CCDWrapper.QueryDisplayFlags.OnlyActivePaths, out numPathArrayElements,
                                                           out numModeInfoArrayElements) == 0)
            {
                // 0 is success.
                var pathInfoArray = new CCDWrapper.DisplayConfigPathInfo[numPathArrayElements];
                var modeInfoArray = new CCDWrapper.DisplayConfigModeInfo[numModeInfoArrayElements];
                var currentTopologyId = CCDWrapper.DisplayConfigTopologyId.Internal;

                var first = Marshal.SizeOf(new CCDWrapper.DisplayConfigPathInfo());
                var second = Marshal.SizeOf(new CCDWrapper.DisplayConfigModeInfo());

                var status = CCDWrapper.QueryDisplayConfig(CCDWrapper.QueryDisplayFlags.DatabaseCurrent,
                                        ref numPathArrayElements, pathInfoArray, ref numModeInfoArrayElements,
                                        modeInfoArray, out currentTopologyId);

                uint dviModeIndex = 0;
                uint hd15ModeIndex = 0;

                foreach (var output in pathInfoArray)
                {
                    if (output.targetInfo.outputTechnology == CCDWrapper.DisplayConfigVideoOutputTechnology.Dvi)
                        dviModeIndex = output.sourceInfo.modeInfoIdx;
                    if (output.targetInfo.outputTechnology == CCDWrapper.DisplayConfigVideoOutputTechnology.Hd15)
                        hd15ModeIndex = output.sourceInfo.modeInfoIdx;
                }

                var dviSourceModeX = modeInfoArray[dviModeIndex].sourceMode.position.x;
                var dviSourceModeY = modeInfoArray[dviModeIndex].sourceMode.position.y;

                var hd15SourceModeX = modeInfoArray[hd15ModeIndex].sourceMode.position.x;
                var hd15SourceModeY = modeInfoArray[hd15ModeIndex].sourceMode.position.y;

                //var hd15ModeInfoArray = new CCDWrapper.DisplayConfigModeInfo();


                // Swap configuration
                if (dviSourceModeX == 0 && dviSourceModeY == 0)
                {

                    modeInfoArray[dviModeIndex].sourceMode.position.x = 1920;
                    modeInfoArray[dviModeIndex].sourceMode.position.y = 0;
                    modeInfoArray[hd15ModeIndex].sourceMode.position.x = 0;
                    modeInfoArray[hd15ModeIndex].sourceMode.position.y = 0;

                    var swap_status = CCDWrapper.SetDisplayConfig(numPathArrayElements, pathInfoArray, numModeInfoArrayElements, modeInfoArray, CCDWrapper.SdcFlags.UseSuppliedDisplayConfig | CCDWrapper.SdcFlags.Apply | CCDWrapper.SdcFlags.SaveToDatabase);

                    modeInfoArray[dviModeIndex].sourceMode.position.x = 0;
                    modeInfoArray[dviModeIndex].sourceMode.position.y = 0;
                    modeInfoArray[hd15ModeIndex].sourceMode.position.x = -1920;
                    modeInfoArray[hd15ModeIndex].sourceMode.position.y = 0;

                    var swap_back_status = CCDWrapper.SetDisplayConfig(numPathArrayElements, pathInfoArray, numModeInfoArrayElements, modeInfoArray, CCDWrapper.SdcFlags.UseSuppliedDisplayConfig | CCDWrapper.SdcFlags.Apply | CCDWrapper.SdcFlags.SaveToDatabase);
                }


                

                Console.WriteLine("Status: {0}", status.ToString());

                var set_status = CCDWrapper.SetDisplayConfig(0, null, 0, null, CCDWrapper.SdcFlags.TopologyExtend | CCDWrapper.SdcFlags.Apply);

            }

        }
    }
}
